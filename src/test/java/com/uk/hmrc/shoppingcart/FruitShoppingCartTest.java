package com.uk.hmrc.shoppingcart;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
/**
 * 
 * @author Ganganna
 *
 */
public class FruitShoppingCartTest {
	FruitShoppingCart cart;
	@Before
	public void setUp(){
	    cart = new FruitShoppingCart();
	}
	
	@Test
	public void cartCheckoutWhenTwoItemsPresent(){
	    List<String> listOfItems = new ArrayList<>();
	    listOfItems.add("Apple");
	    listOfItems.add("Orange");
	    BigDecimal price = cart.fruitBoxCheckout(listOfItems);
	    Assert.assertEquals(price, new BigDecimal(85));
	}

	@Test
	public void cartCheckoutWhenOnly_5_ApplesPresent(){
	    List<String> listOfItems = new ArrayList<>();
	    listOfItems.add("Apple");
	    listOfItems.add("Apple");
	    listOfItems.add("Apple");
	    listOfItems.add("Apple");
	    listOfItems.add("Apple");
	    BigDecimal price = cart.fruitBoxCheckout(listOfItems);
	    Assert.assertEquals(price, new BigDecimal(180));
	}
	
	@Test
	public void cartCheckoutWhenOnly_5_OrangesPresent(){
	    List<String> listOfItems = new ArrayList<>();
	    listOfItems.add("Orange");
	    listOfItems.add("Orange");
	    listOfItems.add("Orange");
	    listOfItems.add("Orange");
	    listOfItems.add("Orange");
	    BigDecimal price = cart.fruitBoxCheckout(listOfItems);
	    Assert.assertEquals(price, new BigDecimal(100));
	}
	
	@Test
	public void cartCheckoutWhenOnly_5_Oranges_And_3_Apples_Present(){
	    List<String> listOfItems = new ArrayList<>();
	    listOfItems.add("Orange");
	    listOfItems.add("Orange");
	    listOfItems.add("Orange");
	    listOfItems.add("Orange");
	    listOfItems.add("Orange");
	    listOfItems.add("Apple");
	    listOfItems.add("Apple");
	    listOfItems.add("Apple");
	    
	    BigDecimal price = cart.fruitBoxCheckout(listOfItems);
	    Assert.assertEquals(price, new BigDecimal(220));
	}
	@Test
	public void cartCheckoutWhenBasketIsEmpty(){
	    List<String> listOfItems = new ArrayList<>();
	    
	    BigDecimal price = cart.fruitBoxCheckout(listOfItems);
	    Assert.assertEquals(price, new BigDecimal(0));
	}
}
