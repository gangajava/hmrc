package com.uk.hmrc.utility;

import java.math.BigDecimal;

import com.uk.hmrc.offers.FruitPriceAndOffers;

public class FruitsUtility {

	public static BigDecimal getApplePrice() {
		
		return new BigDecimal(FruitPriceAndOffers.APPLE_PRICE);
	}
	
	public static BigDecimal getOrangePrice() {
		
		return new BigDecimal(FruitPriceAndOffers.ORANGE_PRICE);
	}
	
	public static String getAppleOffer() {
		
		return FruitPriceAndOffers.APPLE_OFFER;
	}
	
	public static String getOrangeOffer() {
		
		return FruitPriceAndOffers.ORANGE_OFFER;
	}
}
