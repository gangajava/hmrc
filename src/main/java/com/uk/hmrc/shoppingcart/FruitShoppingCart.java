package com.uk.hmrc.shoppingcart;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import com.uk.hmrc.utility.FruitsUtility;

/**
 * 
 * @author Ganganna
 *
 */
public class FruitShoppingCart {
	String appleOffer = FruitsUtility.getAppleOffer();
	String orangesOffer = FruitsUtility.getOrangeOffer();
	BigDecimal applePrice = FruitsUtility.getApplePrice();
	BigDecimal orangePrice = FruitsUtility.getOrangePrice();

	/**
	 * This method is to calucale to price at checkout
	 * @param listOfFruits
	 * @return
	 */
	public BigDecimal fruitBoxCheckout(List<String> listOfFruits) {
		Map<String, Integer> fruitsMap = getTotalFruits(listOfFruits);
		Integer numberOfApples = getTotalApples(fruitsMap);
		Integer numberOfOranges = getTotalOranges(fruitsMap);
		BigDecimal totalApplePrice = numberOfApples != null ? getTotalPriceOfApples(numberOfApples) : new BigDecimal(0);
		BigDecimal totalOrangesPrice = numberOfOranges != null ? getTotalPriceOfOranges(numberOfOranges) : new BigDecimal(0);
		return totalApplePrice.add(totalOrangesPrice);
	}

	/**
	 * This method calculate total prices for given Apples
	 * @param numberOfApples
	 * @return
	 */
	private BigDecimal getTotalPriceOfApples(Integer numberOfApples) {
		BigDecimal totalApplePrice = null;
		if (null != appleOffer && appleOffer.equalsIgnoreCase("Buy one and get one free")) {
			int numberApplesToPrice = numberOfApples - numberOfApples / 2;
			totalApplePrice = applePrice.multiply(new BigDecimal(numberApplesToPrice));
		} else if (null == appleOffer) {
			totalApplePrice = applePrice.multiply(new BigDecimal(numberOfApples));
		}
		return totalApplePrice;
	}

	/**
	 * This method calculate total prices for given Oranges
	 * @param numberOfOranges
	 * @return
	 */
	private BigDecimal getTotalPriceOfOranges(Integer numberOfOranges) {
		BigDecimal totalOrangesPrice = new BigDecimal(0);
		if (null != orangesOffer && orangesOffer.equalsIgnoreCase("3 for price of 2")) {
			int numberOrangesToPrice = numberOfOranges - numberOfOranges / 3;
			totalOrangesPrice = orangePrice.multiply(new BigDecimal(numberOrangesToPrice));
		} else if (null == orangesOffer) {
			totalOrangesPrice = orangePrice.multiply(new BigDecimal(numberOfOranges));
		}

		return totalOrangesPrice;
	}

	/**
	 * This give total fruits by type
	 * @param listOfFruits
	 * @return
	 */
	private Map<String, Integer> getTotalFruits(List<String> listOfFruits) {
		AtomicInteger appleCount = new AtomicInteger();
		AtomicInteger orangeCount = new AtomicInteger();
		Map<String, Integer> map = new HashMap<String, Integer>();
		Iterator<String> itr = listOfFruits.iterator();

		while (itr.hasNext()) {
			if (itr.next().equals("Apple")) {
				map.put("Apple", appleCount.incrementAndGet());
			} else {
				map.put("Orange", orangeCount.incrementAndGet());
			}
		}
		return map;
	}

	/**
	 * This method give total number Apples from Basket
	 * @param fruitsMap
	 * @return
	 */
	private Integer getTotalApples(Map<String, Integer> fruitsMap) {

		return fruitsMap.get("Apple");
	}

	/**
	 * This method give total number Oranges from Basket
	 * @param fruitsMap
	 * @return
	 */
	private Integer getTotalOranges(Map<String, Integer> fruitsMap) {

		return fruitsMap.get("Orange");
	}
}
